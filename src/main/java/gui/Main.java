package gui;

import cellular.CellAutomaton;
import cellular.GameOfLife;
import cellular.BriansBrain;

public class Main {

	public static void main(String[] args) {
		CellAutomaton ca = new GameOfLife(100,100);
		CellAutomaton bb = new BriansBrain(100, 100);
		CellAutomataGUI.run(ca);
		CellAutomataGUI.run(bb);
	}

}
