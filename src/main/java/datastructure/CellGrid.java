package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int cols;
    int rows;
    CellState[][] grid;

    public CellGrid(int rows, int cols, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.cols = cols;
        this.rows = rows;
        this.grid = new CellState[rows][cols];


	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= rows || column < 0 || column >= cols) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= rows || column < 0 || column >= cols) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid gridCopy = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++){
                gridCopy.set(i, j, this.get(i,j));
            }
        }
        return gridCopy;
    }
    
}
